idem_test_vm_is_present:
  vcenter.vm.present:
  - guest_os: string
  - placement:
      cluster: string
      datastore: string
      folder: string
      host: string
      resource_pool: string
  - hardware_version: string
  - boot:
      delay: int
      efi_legacy_boot: bool
      enter_setup_mode: bool
      network_protocol: string
      retry: bool
      retry_delay: int
      type_: string
  - boot_devices:
    - type_: string
  - cpu:
      cores_per_socket: int
      count: int
      hot_add_enabled: bool
      hot_remove_enabled: bool
  - memory:
      hot_add_enabled: bool
      size_mi_b: int
  - disks:
    - backing:
        type_: string
        vmdk_file: string
      ide:
        master: bool
        primary: bool
      new_vmdk:
        capacity: int
        name: string
        storage_policy:
          policy: string
      nvme:
        bus: int
        unit: int
      sata:
        bus: int
        unit: int
      scsi:
        bus: int
        unit: int
      type_: string
  - nics:
    - allow_guest_control: bool
      backing:
        distributed_port: string
        network: string
        type_: string
      mac_address: string
      mac_type: string
      pci_slot_number: int
      start_connected: bool
      type_: string
      upt_compatibility_enabled: bool
      wake_on_lan_enabled: bool
  - cdroms:
    - allow_guest_control: bool
      backing:
        device_access_type: string
        host_device: string
        iso_file: string
        type_: string
      ide:
        master: bool
        primary: bool
      sata:
        bus: int
        unit: int
      start_connected: bool
      type_: string
  - floppies:
    - allow_guest_control: bool
      backing:
        host_device: string
        image_file: string
        type_: string
      start_connected: bool
  - parallel_ports:
    - allow_guest_control: bool
      backing:
        file: string
        host_device: string
        type_: string
      start_connected: bool
  - serial_ports:
    - allow_guest_control: bool
      backing:
        file: string
        host_device: string
        network_location: string
        no_rx_loss: bool
        pipe: string
        proxy: string
        type_: string
      start_connected: bool
      yield_on_poll: bool
  - sata_adapters:
    - bus: int
      pci_slot_number: int
      type_: string
  - scsi_adapters:
    - bus: int
      pci_slot_number: int
      sharing: string
      type_: string
  - nvme_adapters:
    - bus: int
      pci_slot_number: int
  - storage_policy:
      policy: string

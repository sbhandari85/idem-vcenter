def __init__(hub):
    # This enables acct profiles that begin with "vcenter" for states
    hub.states.vcenter.ACCT = ["vcenter"]

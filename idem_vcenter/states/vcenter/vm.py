"""States module for managing Vms. """
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

import dict_tools.differ as differ

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    guest_os: str,
    resource_id: str = None,
    placement: make_dataclass(
        "placement",
        [
            ("folder", str, field(default=None)),
            ("resource_pool", str, field(default=None)),
            ("host", str, field(default=None)),
            ("cluster", str, field(default=None)),
            ("datastore", str, field(default=None)),
        ],
    ) = None,
    hardware_version: str = None,
    boot: make_dataclass(
        "boot",
        [
            ("type", str, field(default=None)),
            ("efi_legacy_boot", bool, field(default=None)),
            ("network_protocol", str, field(default=None)),
            ("delay", int, field(default=None)),
            ("retry", bool, field(default=None)),
            ("retry_delay", int, field(default=None)),
            ("enter_setup_mode", bool, field(default=None)),
        ],
    ) = None,
    boot_devices: List[make_dataclass("boot_devices", [("type", str)])] = None,
    cpu: make_dataclass(
        "cpu",
        [
            ("count", int, field(default=None)),
            ("cores_per_socket", int, field(default=None)),
            ("hot_add_enabled", bool, field(default=None)),
            ("hot_remove_enabled", bool, field(default=None)),
        ],
    ) = None,
    memory: make_dataclass(
        "memory",
        [
            ("size_mi_b", int, field(default=None)),
            ("hot_add_enabled", bool, field(default=None)),
        ],
    ) = None,
    disks: List[
        make_dataclass(
            "disks",
            [
                ("type", str, field(default=None)),
                (
                    "ide",
                    make_dataclass(
                        "ide",
                        [
                            ("primary", bool, field(default=None)),
                            ("master", bool, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                (
                    "scsi",
                    make_dataclass(
                        "scsi", [("bus", int), ("unit", int, field(default=None))]
                    ),
                    field(default=None),
                ),
                (
                    "sata",
                    make_dataclass(
                        "sata", [("bus", int), ("unit", int, field(default=None))]
                    ),
                    field(default=None),
                ),
                (
                    "nvme",
                    make_dataclass(
                        "nvme", [("bus", int), ("unit", int, field(default=None))]
                    ),
                    field(default=None),
                ),
                (
                    "backing",
                    make_dataclass(
                        "backing",
                        [("type", str), ("vmdk_file", str, field(default=None))],
                    ),
                    field(default=None),
                ),
                (
                    "new_vmdk",
                    make_dataclass(
                        "new_vmdk",
                        [
                            ("name", str, field(default=None)),
                            ("capacity", int, field(default=None)),
                            (
                                "storage_policy",
                                make_dataclass("storage_policy", [("policy", str)]),
                                field(default=None),
                            ),
                        ],
                    ),
                    field(default=None),
                ),
            ],
        )
    ] = None,
    nics: List[
        make_dataclass(
            "nics",
            [
                ("type", str, field(default=None)),
                ("upt_compatibility_enabled", bool, field(default=None)),
                ("mac_type", str, field(default=None)),
                ("mac_address", str, field(default=None)),
                ("pci_slot_number", int, field(default=None)),
                ("wake_on_lan_enabled", bool, field(default=None)),
                (
                    "backing",
                    make_dataclass(
                        "backing",
                        [
                            ("type", str),
                            ("network", str, field(default=None)),
                            ("distributed_port", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("start_connected", bool, field(default=None)),
                ("allow_guest_control", bool, field(default=None)),
            ],
        )
    ] = None,
    cdroms: List[
        make_dataclass(
            "cdroms",
            [
                ("type", str, field(default=None)),
                (
                    "ide",
                    make_dataclass(
                        "ide",
                        [
                            ("primary", bool, field(default=None)),
                            ("master", bool, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                (
                    "sata",
                    make_dataclass(
                        "sata", [("bus", int), ("unit", int, field(default=None))]
                    ),
                    field(default=None),
                ),
                (
                    "backing",
                    make_dataclass(
                        "backing",
                        [
                            ("type", str),
                            ("iso_file", str, field(default=None)),
                            ("host_device", str, field(default=None)),
                            ("device_access_type", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("start_connected", bool, field(default=None)),
                ("allow_guest_control", bool, field(default=None)),
            ],
        )
    ] = None,
    floppies: List[
        make_dataclass(
            "floppies",
            [
                (
                    "backing",
                    make_dataclass(
                        "backing",
                        [
                            ("type", str),
                            ("image_file", str, field(default=None)),
                            ("host_device", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("start_connected", bool, field(default=None)),
                ("allow_guest_control", bool, field(default=None)),
            ],
        )
    ] = None,
    parallel_ports: List[
        make_dataclass(
            "parallel_ports",
            [
                (
                    "backing",
                    make_dataclass(
                        "backing",
                        [
                            ("type", str),
                            ("file", str, field(default=None)),
                            ("host_device", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("start_connected", bool, field(default=None)),
                ("allow_guest_control", bool, field(default=None)),
            ],
        )
    ] = None,
    serial_ports: List[
        make_dataclass(
            "serial_ports",
            [
                ("yield_on_poll", bool, field(default=None)),
                (
                    "backing",
                    make_dataclass(
                        "backing",
                        [
                            ("type", str),
                            ("file", str, field(default=None)),
                            ("host_device", str, field(default=None)),
                            ("pipe", str, field(default=None)),
                            ("no_rx_loss", bool, field(default=None)),
                            ("network_location", str, field(default=None)),
                            ("proxy", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("start_connected", bool, field(default=None)),
                ("allow_guest_control", bool, field(default=None)),
            ],
        )
    ] = None,
    sata_adapters: List[
        make_dataclass(
            "sata_adapters",
            [
                ("type", str, field(default=None)),
                ("bus", int, field(default=None)),
                ("pci_slot_number", int, field(default=None)),
            ],
        )
    ] = None,
    scsi_adapters: List[
        make_dataclass(
            "scsi_adapters",
            [
                ("type", str, field(default=None)),
                ("bus", int, field(default=None)),
                ("pci_slot_number", int, field(default=None)),
                ("sharing", str, field(default=None)),
            ],
        )
    ] = None,
    nvme_adapters: List[
        make_dataclass(
            "nvme_adapters",
            [
                ("bus", int, field(default=None)),
                ("pci_slot_number", int, field(default=None)),
            ],
        )
    ] = None,
    storage_policy: make_dataclass("storage_policy", [("policy", str)]) = None,
) -> Dict[str, Any]:
    """
    Creates a virtual machine.
        if you do not have all of the privileges described as follows:
        -  The resource Folder referenced by the attribute VM.InventoryPlacementSpec.folder requires VirtualMachine.Inventory.Create.
        -  The resource ResourcePool referenced by the attribute VM.ComputePlacementSpec.resource-pool requires Resource.AssignVMToPool.
        -  The resource Datastore referenced by the attribute VM.StoragePlacementSpec.datastore requires Datastore.AllocateSpace.
        -  The resource Network referenced by the attribute Ethernet.BackingSpec.network requires Network.Assign.

        None

    Args:
        name(str):
            Idem name of the resource.

        guest_os(str):
            The GuestOS enumerated type defines the valid guest operating system types used for configuring a virtual machine.

        resource_id(str, Optional):
            Vm unique ID. Defaults to None.

        placement(dict[str, Any], Optional):
            placement. Defaults to None.

            * folder (str, Optional):
                Virtual machine folder into which the virtual machine should be placed.
                This field is currently required. In the future, if this field is unset, the system will attempt to choose a suitable folder for the virtual machine; if a folder cannot be chosen, the virtual machine creation operation will fail.
                When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: Folder. When operations return a value of this structure as a result, the field will be an identifier for the resource type: Folder. Defaults to None.

            * resource_pool (str, Optional):
                Resource pool into which the virtual machine should be placed.
                This field is currently required if both VM.ComputePlacementSpec.host and VM.ComputePlacementSpec.cluster are unset. In the future, if this field is unset, the system will attempt to choose a suitable resource pool for the virtual machine; if a resource pool cannot be chosen, the virtual machine creation operation will fail.
                When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: ResourcePool. When operations return a value of this structure as a result, the field will be an identifier for the resource type: ResourcePool. Defaults to None.

            * host (str, Optional):
                Host onto which the virtual machine should be placed.
                 If VM.ComputePlacementSpec.host and VM.ComputePlacementSpec.resource-pool are both specified, VM.ComputePlacementSpec.resource-pool must belong to VM.ComputePlacementSpec.host.

                 If VM.ComputePlacementSpec.host and VM.ComputePlacementSpec.cluster are both specified, VM.ComputePlacementSpec.host must be a member of VM.ComputePlacementSpec.cluster.

                This field may be unset if VM.ComputePlacementSpec.resource-pool or VM.ComputePlacementSpec.cluster is specified. If unset, the system will attempt to choose a suitable host for the virtual machine; if a host cannot be chosen, the virtual machine creation operation will fail.
                When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: HostSystem. When operations return a value of this structure as a result, the field will be an identifier for the resource type: HostSystem. Defaults to None.

            * cluster (str, Optional):
                Cluster into which the virtual machine should be placed.
                 If VM.ComputePlacementSpec.cluster and VM.ComputePlacementSpec.resource-pool are both specified, VM.ComputePlacementSpec.resource-pool must belong to VM.ComputePlacementSpec.cluster.

                 If VM.ComputePlacementSpec.cluster and VM.ComputePlacementSpec.host are both specified, VM.ComputePlacementSpec.host must be a member of VM.ComputePlacementSpec.cluster.

                If VM.ComputePlacementSpec.resource-pool or VM.ComputePlacementSpec.host is specified, it is recommended that this field be unset.
                When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: ClusterComputeResource. When operations return a value of this structure as a result, the field will be an identifier for the resource type: ClusterComputeResource. Defaults to None.

            * datastore (str, Optional):
                Datastore on which the virtual machine's configuration state should be stored. This datastore will also be used for any virtual disks that are created as part of the virtual machine creation operation.
                This field is currently required. In the future, if this field is unset, the system will attempt to choose suitable storage for the virtual machine; if storage cannot be chosen, the virtual machine creation operation will fail.
                When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: Datastore. When operations return a value of this structure as a result, the field will be an identifier for the resource type: Datastore. Defaults to None.

        hardware_version(str, Optional):
            The Hardware.Version enumerated type defines the valid virtual hardware versions for a virtual machine. See https://kb.vmware.com/s/article/1003746 (Virtual machine hardware versions (1003746)). Defaults to None.

        boot(dict[str, Any], Optional):
            boot. Defaults to None.

            * type (str, Optional):
                The Boot.Type enumerated type defines the valid firmware types for a virtual machine. Defaults to None.

            * efi_legacy_boot (bool, Optional):
                Flag indicating whether to use EFI legacy boot mode.
                If unset, defaults to value that is recommended for the guest OS and is supported for the virtual hardware version. Defaults to None.

            * network_protocol (str, Optional):
                The Boot.NetworkProtocol enumerated type defines the valid network boot protocols supported when booting a virtual machine with EFI firmware over the network. Defaults to None.

            * delay (int, Optional):
                Delay in milliseconds before beginning the firmware boot process when the virtual machine is powered on. This delay may be used to provide a time window for users to connect to the virtual machine console and enter BIOS setup mode.
                If unset, default value is 0. Defaults to None.

            * retry (bool, Optional):
                Flag indicating whether the virtual machine should automatically retry the boot process after a failure.
                If unset, default value is false. Defaults to None.

            * retry_delay (int, Optional):
                Delay in milliseconds before retrying the boot process after a failure; applicable only when Boot.Info.retry is true.
                If unset, default value is 10000. Defaults to None.

            * enter_setup_mode (bool, Optional):
                Flag indicating whether the firmware boot process should automatically enter setup mode the next time the virtual machine boots. Note that this flag will automatically be reset to false once the virtual machine enters setup mode.
                If unset, the value is unchanged. Defaults to None.

        boot_devices(List[dict[str, Any]], Optional):
            Boot device configuration.
            If unset, a server-specific boot sequence will be used. Defaults to None.

            * type (str):
                The Device.Type enumerated type defines the valid device types that may be used as bootable devices.

        cpu(dict[str, Any], Optional):
            cpu. Defaults to None.

            * count (int, Optional):
                New number of CPU cores. The number of CPU cores in the virtual machine must be a multiple of the number of cores per socket.
                 The supported range of CPU counts is constrained by the configured guest operating system and virtual hardware version of the virtual machine.

                 If the virtual machine is running, the number of CPU cores may only be increased if Cpu.Info.hot-add-enabled is true, and may only be decreased if Cpu.Info.hot-remove-enabled is true.

                If unset, the value is unchanged. Defaults to None.

            * cores_per_socket (int, Optional):
                New number of CPU cores per socket. The number of CPU cores in the virtual machine must be a multiple of the number of cores per socket.
                If unset, the value is unchanged. Defaults to None.

            * hot_add_enabled (bool, Optional):
                Flag indicating whether adding CPUs while the virtual machine is running is enabled.
                 This field may only be modified if the virtual machine is powered off.

                If unset, the value is unchanged. Defaults to None.

            * hot_remove_enabled (bool, Optional):
                Flag indicating whether removing CPUs while the virtual machine is running is enabled.
                 This field may only be modified if the virtual machine is powered off.

                If unset, the value is unchanged. Defaults to None.

        memory(dict[str, Any], Optional):
            memory. Defaults to None.

            * size_mi_b (int, Optional):
                New memory size in mebibytes.
                 The supported range of memory sizes is constrained by the configured guest operating system and virtual hardware version of the virtual machine.

                 If the virtual machine is running, this value may only be changed if Memory.Info.hot-add-enabled is true, and the new memory size must satisfy the constraints specified by Memory.Info.hot-add-increment-size-mib and Memory.Info.hot-add-limit-mib.

                If unset, the value is unchanged. Defaults to None.

            * hot_add_enabled (bool, Optional):
                Flag indicating whether adding memory while the virtual machine is running should be enabled.
                 Some guest operating systems may consume more resources or perform less efficiently when they run on hardware that supports adding memory while the machine is running.

                 This field may only be modified if the virtual machine is not powered on.

                If unset, the value is unchanged. Defaults to None.

        disks(List[dict[str, Any]], Optional):
            List of disks.
            If unset, a single blank virtual disk of a guest-specific size will be created on the same storage as the virtual machine configuration, and will use a guest-specific host bus adapter type. If the guest-specific size is 0, no virtual disk will be created. Defaults to None.

            * type (str, Optional):
                The Disk.HostBusAdapterType enumerated type defines the valid types of host bus adapters that may be used for attaching a virtual storage device to a virtual machine. Defaults to None.

            * ide (dict[str, Any], Optional):
                ide. Defaults to None.

                * primary (bool, Optional):
                    Flag specifying whether the device should be attached to the primary or secondary IDE adapter of the virtual machine.
                    If unset, the server will choose a adapter with an available connection. If no IDE connections are available, the request will be rejected. Defaults to None.

                * master (bool, Optional):
                    Flag specifying whether the device should be the master or slave device on the IDE adapter.
                    If unset, the server will choose an available connection type. If no IDE connections are available, the request will be rejected. Defaults to None.

            * scsi (dict[str, Any], Optional):
                scsi. Defaults to None.

                * bus (int):
                    Bus number of the adapter to which the device should be attached.

                * unit (int, Optional):
                    Unit number of the device.
                    If unset, the server will choose an available unit number on the specified adapter. If there are no available connections on the adapter, the request will be rejected. Defaults to None.

            * sata (dict[str, Any], Optional):
                sata. Defaults to None.

                * bus (int):
                    Bus number of the adapter to which the device should be attached.

                * unit (int, Optional):
                    Unit number of the device.
                    If unset, the server will choose an available unit number on the specified adapter. If there are no available connections on the adapter, the request will be rejected. Defaults to None.

            * nvme (dict[str, Any], Optional):
                nvme. Defaults to None.

                * bus (int):
                    Bus number of the adapter to which the device should be attached.

                * unit (int, Optional):
                    Unit number of the device.
                    If unset, the server will choose an available unit number on the specified adapter. If there are no available connections on the adapter, the request will be rejected. Defaults to None.

            * backing (dict[str, Any], Optional):
                backing. Defaults to None.

                * type (str):
                    The Disk.BackingType enumerated type defines the valid backing types for a virtual disk.

                * vmdk_file (str, Optional):
                    Path of the VMDK file backing the virtual disk.
                    This field is optional and it is only relevant when the value of Disk.BackingSpec.type is VMDK_FILE. Defaults to None.

            * new_vmdk (dict[str, Any], Optional):
                new_vmdk. Defaults to None.

                * name (str, Optional):
                    Base name of the VMDK file. The name should not include the '.vmdk' file extension.
                    If unset, a name (derived from the name of the virtual machine) will be chosen by the server. Defaults to None.

                * capacity (int, Optional):
                    Capacity of the virtual disk backing in bytes.
                    If unset, defaults to a guest-specific capacity. Defaults to None.

                * storage_policy (dict[str, Any], Optional):
                    storage_policy. Defaults to None.

                    * policy (str):
                        Identifier of the storage policy which should be associated with the VMDK file.
                        When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: vcenter.StoragePolicy. When operations return a value of this structure as a result, the field will be an identifier for the resource type: vcenter.StoragePolicy.

        nics(List[dict[str, Any]], Optional):
            List of Ethernet adapters.
            If unset, no Ethernet adapters will be created. Defaults to None.

            * type (str, Optional):
                The Ethernet.EmulationType enumerated type defines the valid emulation types for a virtual Ethernet adapter. Defaults to None.

            * upt_compatibility_enabled (bool, Optional):
                Flag indicating whether Universal Pass-Through (UPT) compatibility is enabled on this virtual Ethernet adapter.
                If unset, defaults to false. Defaults to None.

            * mac_type (str, Optional):
                The Ethernet.MacAddressType enumerated type defines the valid MAC address origins for a virtual Ethernet adapter. Defaults to None.

            * mac_address (str, Optional):
                MAC address.
                Workaround for PR1459647. Defaults to None.

            * pci_slot_number (int, Optional):
                Address of the virtual Ethernet adapter on the PCI bus. If the PCI address is invalid, the server will change when it the VM is started or as the device is hot added.
                If unset, the server will choose an available address when the virtual machine is powered on. Defaults to None.

            * wake_on_lan_enabled (bool, Optional):
                Flag indicating whether wake-on-LAN is enabled on this virtual Ethernet adapter.
                Defaults to false if unset. Defaults to None.

            * backing (dict[str, Any], Optional):
                backing. Defaults to None.

                * type (str):
                    The Ethernet.BackingType enumerated type defines the valid backing types for a virtual Ethernet adapter.

                * network (str, Optional):
                    Identifier of the network that backs the virtual Ethernet adapter.
                    This field is optional and it is only relevant when the value of Ethernet.BackingSpec.type is one of STANDARD_PORTGROUP, DISTRIBUTED_PORTGROUP, or OPAQUE_NETWORK.
                    When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: Network. When operations return a value of this structure as a result, the field will be an identifier for the resource type: Network. Defaults to None.

                * distributed_port (str, Optional):
                    Key of the distributed virtual port that backs the virtual Ethernet adapter. Depending on the type of the Portgroup, the port may be specified using this field. If the portgroup type is early-binding (also known as static), a port is assigned when the Ethernet adapter is configured to use the port. The port may be either automatically or specifically assigned based on the value of this field. If the portgroup type is ephemeral, the port is created and assigned to a virtual machine when it is powered on and the Ethernet adapter is connected. This field cannot be specified as no free ports exist before use.
                    May be used to specify a port when the network specified on the Ethernet.BackingSpec.network field is a static or early binding distributed portgroup. If unset, the port will be automatically assigned to the Ethernet adapter based on the policy embodied by the portgroup type. Defaults to None.

            * start_connected (bool, Optional):
                Flag indicating whether the virtual device should be connected whenever the virtual machine is powered on.
                Defaults to false if unset. Defaults to None.

            * allow_guest_control (bool, Optional):
                Flag indicating whether the guest can connect and disconnect the device.
                Defaults to false if unset. Defaults to None.

        cdroms(List[dict[str, Any]], Optional):
            List of CD-ROMs.
            If unset, no CD-ROM devices will be created. Defaults to None.

            * type (str, Optional):
                The Cdrom.HostBusAdapterType enumerated type defines the valid types of host bus adapters that may be used for attaching a Cdrom to a virtual machine. Defaults to None.

            * ide (dict[str, Any], Optional):
                ide. Defaults to None.

                * primary (bool, Optional):
                    Flag specifying whether the device should be attached to the primary or secondary IDE adapter of the virtual machine.
                    If unset, the server will choose a adapter with an available connection. If no IDE connections are available, the request will be rejected. Defaults to None.

                * master (bool, Optional):
                    Flag specifying whether the device should be the master or slave device on the IDE adapter.
                    If unset, the server will choose an available connection type. If no IDE connections are available, the request will be rejected. Defaults to None.

            * sata (dict[str, Any], Optional):
                sata. Defaults to None.

                * bus (int):
                    Bus number of the adapter to which the device should be attached.

                * unit (int, Optional):
                    Unit number of the device.
                    If unset, the server will choose an available unit number on the specified adapter. If there are no available connections on the adapter, the request will be rejected. Defaults to None.

            * backing (dict[str, Any], Optional):
                backing. Defaults to None.

                * type (str):
                    The Cdrom.BackingType enumerated type defines the valid backing types for a virtual CD-ROM device.

                * iso_file (str, Optional):
                    Path of the image file that should be used as the virtual CD-ROM device backing.
                    This field is optional and it is only relevant when the value of Cdrom.BackingSpec.type is ISO_FILE. Defaults to None.

                * host_device (str, Optional):
                    Name of the device that should be used as the virtual CD-ROM device backing.
                    If unset, the virtual CD-ROM device will be configured to automatically detect a suitable host device. Defaults to None.

                * device_access_type (str, Optional):
                    The Cdrom.DeviceAccessType enumerated type defines the valid device access types for a physical device packing of a virtual CD-ROM device. Defaults to None.

            * start_connected (bool, Optional):
                Flag indicating whether the virtual device should be connected whenever the virtual machine is powered on.
                Defaults to false if unset. Defaults to None.

            * allow_guest_control (bool, Optional):
                Flag indicating whether the guest can connect and disconnect the device.
                Defaults to false if unset. Defaults to None.

        floppies(List[dict[str, Any]], Optional):
            List of floppy drives.
            If unset, no floppy drives will be created. Defaults to None.

            * backing (dict[str, Any], Optional):
                backing. Defaults to None.

                * type (str):
                    The Floppy.BackingType enumerated type defines the valid backing types for a virtual floppy drive.

                * image_file (str, Optional):
                    Path of the image file that should be used as the virtual floppy drive backing.
                    This field is optional and it is only relevant when the value of Floppy.BackingSpec.type is IMAGE_FILE. Defaults to None.

                * host_device (str, Optional):
                    Name of the device that should be used as the virtual floppy drive backing.
                    If unset, the virtual floppy drive will be configured to automatically detect a suitable host device. Defaults to None.

            * start_connected (bool, Optional):
                Flag indicating whether the virtual device should be connected whenever the virtual machine is powered on.
                Defaults to false if unset. Defaults to None.

            * allow_guest_control (bool, Optional):
                Flag indicating whether the guest can connect and disconnect the device.
                Defaults to false if unset. Defaults to None.

        parallel_ports(List[dict[str, Any]], Optional):
            List of parallel ports.
            If unset, no parallel ports will be created. Defaults to None.

            * backing (dict[str, Any], Optional):
                backing. Defaults to None.

                * type (str):
                    The Parallel.BackingType enumerated type defines the valid backing types for a virtual parallel port.

                * file (str, Optional):
                    Path of the file that should be used as the virtual parallel port backing.
                    This field is optional and it is only relevant when the value of Parallel.BackingSpec.type is FILE. Defaults to None.

                * host_device (str, Optional):
                    Name of the device that should be used as the virtual parallel port backing.
                    If unset, the virtual parallel port will be configured to automatically detect a suitable host device. Defaults to None.

            * start_connected (bool, Optional):
                Flag indicating whether the virtual device should be connected whenever the virtual machine is powered on.
                Defaults to false if unset. Defaults to None.

            * allow_guest_control (bool, Optional):
                Flag indicating whether the guest can connect and disconnect the device.
                Defaults to false if unset. Defaults to None.

        serial_ports(List[dict[str, Any]], Optional):
            List of serial ports.
            If unset, no serial ports will be created. Defaults to None.

            * yield_on_poll (bool, Optional):
                CPU yield behavior. If set to true, the virtual machine will periodically relinquish the processor if its sole task is polling the virtual serial port. The amount of time it takes to regain the processor will depend on the degree of other virtual machine activity on the host.
                If unset, defaults to false. Defaults to None.

            * backing (dict[str, Any], Optional):
                backing. Defaults to None.

                * type (str):
                    The Serial.BackingType enumerated type defines the valid backing types for a virtual serial port.

                * file (str, Optional):
                    Path of the file backing the virtual serial port.
                    This field is optional and it is only relevant when the value of Serial.BackingSpec.type is FILE. Defaults to None.

                * host_device (str, Optional):
                    Name of the device backing the virtual serial port.


                    If unset, the virtual serial port will be configured to automatically detect a suitable host device. Defaults to None.

                * pipe (str, Optional):
                    Name of the pipe backing the virtual serial port.
                    This field is optional and it is only relevant when the value of Serial.BackingSpec.type is one of PIPE_SERVER or PIPE_CLIENT. Defaults to None.

                * no_rx_loss (bool, Optional):
                    Flag that enables optimized data transfer over the pipe. When the value is true, the host buffers data to prevent data overrun. This allows the virtual machine to read all of the data transferred over the pipe with no data loss.
                    If unset, defaults to false. Defaults to None.

                * network_location (str, Optional):
                    URI specifying the location of the network service backing the virtual serial port.
                       - If Serial.BackingSpec.type is NETWORK_SERVER, this field is the location used by clients to connect to this server. The hostname part of the URI should either be empty or should specify the address of the host on which the virtual machine is running.
                       - If Serial.BackingSpec.type is NETWORK_CLIENT, this field is the location used by the virtual machine to connect to the remote server.

                    This field is optional and it is only relevant when the value of Serial.BackingSpec.type is one of NETWORK_SERVER or NETWORK_CLIENT. Defaults to None.

                * proxy (str, Optional):
                    Proxy service that provides network access to the network backing. If set, the virtual machine initiates a connection with the proxy service and forwards the traffic to the proxy.
                    If unset, no proxy service should be used. Defaults to None.

            * start_connected (bool, Optional):
                Flag indicating whether the virtual device should be connected whenever the virtual machine is powered on.
                Defaults to false if unset. Defaults to None.

            * allow_guest_control (bool, Optional):
                Flag indicating whether the guest can connect and disconnect the device.
                Defaults to false if unset. Defaults to None.

        sata_adapters(List[dict[str, Any]], Optional):
            List of SATA adapters.
            If unset, any adapters necessary to connect the virtual machine's storage devices will be created; this includes any devices that explicitly specify a SATA host bus adapter, as well as any devices that do not specify a host bus adapter if the guest's preferred adapter type is SATA. Defaults to None.

            * type (str, Optional):
                The Sata.Type enumerated type defines the valid emulation types for a virtual SATA adapter. Defaults to None.

            * bus (int, Optional):
                SATA bus number.
                If unset, the server will choose an available bus number; if none is available, the request will fail. Defaults to None.

            * pci_slot_number (int, Optional):
                Address of the SATA adapter on the PCI bus.
                If unset, the server will choose an available address when the virtual machine is powered on. Defaults to None.

        scsi_adapters(List[dict[str, Any]], Optional):
            List of SCSI adapters.
            If unset, any adapters necessary to connect the virtual machine's storage devices will be created; this includes any devices that explicitly specify a SCSI host bus adapter, as well as any devices that do not specify a host bus adapter if the guest's preferred adapter type is SCSI. The type of the SCSI adapter will be a guest-specific default type. Defaults to None.

            * type (str, Optional):
                The Scsi.Type enumerated type defines the valid emulation types for a virtual SCSI adapter. Defaults to None.

            * bus (int, Optional):
                SCSI bus number.
                If unset, the server will choose an available bus number; if none is available, the request will fail. Defaults to None.

            * pci_slot_number (int, Optional):
                Address of the SCSI adapter on the PCI bus. If the PCI address is invalid, the server will change it when the VM is started or as the device is hot added.
                If unset, the server will choose an available address when the virtual machine is powered on. Defaults to None.

            * sharing (str, Optional):
                The Scsi.Sharing enumerated type defines the valid bus sharing modes for a virtual SCSI adapter. Defaults to None.

        nvme_adapters(List[dict[str, Any]], Optional):
            List of NVMe adapters.
            If unset, any adapters necessary to connect the virtual machine's storage devices will be created; this includes any devices that explicitly specify a NVMe host bus adapter, as well as any devices that do not specify a host bus adapter if the guest's preferred adapter type is NVMe. Defaults to None.

            * bus (int, Optional):
                NVMe bus number.
                If unset, the server will choose an available bus number; if none is available, the request will fail. Defaults to None.

            * pci_slot_number (int, Optional):
                Address of the NVMe adapter on the PCI bus.
                If unset, the server will choose an available address when the virtual machine is powered on. Defaults to None.

        storage_policy(dict[str, Any], Optional):
            storage_policy. Defaults to None.

            * policy (str):
                Identifier of the storage policy which should be associated with the virtual machine.
                When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: vcenter.StoragePolicy. When operations return a value of this structure as a result, the field will be an identifier for the resource type: vcenter.StoragePolicy.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls


          idem_test_vcenter.vm_is_present:
              vcenter.vcenter.vm.present:
              - guest_os: string
              - placement:
                  cluster: string
                  datastore: string
                  folder: string
                  host: string
                  resource_pool: string
              - hardware_version: string
              - boot:
                  delay: int
                  efi_legacy_boot: bool
                  enter_setup_mode: bool
                  network_protocol: string
                  retry: bool
                  retry_delay: int
                  type_: string
              - boot_devices:
                - type_: string
              - cpu:
                  cores_per_socket: int
                  count: int
                  hot_add_enabled: bool
                  hot_remove_enabled: bool
              - memory:
                  hot_add_enabled: bool
                  size_mi_b: int
              - disks:
                - backing:
                    type_: string
                    vmdk_file: string
                  ide:
                    master: bool
                    primary: bool
                  new_vmdk:
                    capacity: int
                    name: string
                    storage_policy:
                      policy: string
                  nvme:
                    bus: int
                    unit: int
                  sata:
                    bus: int
                    unit: int
                  scsi:
                    bus: int
                    unit: int
                  type_: string
              - nics:
                - allow_guest_control: bool
                  backing:
                    distributed_port: string
                    network: string
                    type_: string
                  mac_address: string
                  mac_type: string
                  pci_slot_number: int
                  start_connected: bool
                  type_: string
                  upt_compatibility_enabled: bool
                  wake_on_lan_enabled: bool
              - cdroms:
                - allow_guest_control: bool
                  backing:
                    device_access_type: string
                    host_device: string
                    iso_file: string
                    type_: string
                  ide:
                    master: bool
                    primary: bool
                  sata:
                    bus: int
                    unit: int
                  start_connected: bool
                  type_: string
              - floppies:
                - allow_guest_control: bool
                  backing:
                    host_device: string
                    image_file: string
                    type_: string
                  start_connected: bool
              - parallel_ports:
                - allow_guest_control: bool
                  backing:
                    file: string
                    host_device: string
                    type_: string
                  start_connected: bool
              - serial_ports:
                - allow_guest_control: bool
                  backing:
                    file: string
                    host_device: string
                    network_location: string
                    no_rx_loss: bool
                    pipe: string
                    proxy: string
                    type_: string
                  start_connected: bool
                  yield_on_poll: bool
              - sata_adapters:
                - bus: int
                  pci_slot_number: int
                  type_: string
              - scsi_adapters:
                - bus: int
                  pci_slot_number: int
                  sharing: string
                  type_: string
              - nvme_adapters:
                - bus: int
                  pci_slot_number: int
              - storage_policy:
                  policy: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "kwargs", "result") and v is not None
    }

    if resource_id:
        before = await hub.exec.vcenter.vm.get(
            ctx,
            name=name,
            resource_id=resource_id,
        )

        if not before["result"] or not before["ret"]:
            result["result"] = False
            result["comment"] = before["comment"]
            return result

        result["old_state"] = before.ret

        result["comment"].append(f"'vcenter.vm: {name}' already exists")

        # remove placement from desired state
        del desired_state["placement"]
        del desired_state["guest_os"]

        # If there are changes in desired state from existing state
        changes = differ.deep_diff(before.ret if before.ret else {}, desired_state)

        if bool(changes.get("new")):
            if ctx.test:
                result[
                    "new_state"
                ] = hub.tool.vcenter.test_state_utils.generate_test_state(
                    enforced_state={}, desired_state=desired_state
                )
                result["comment"].append(f"Would update vcenter.vm: {name}")
                return result
            else:
                # Update the resource - It is a no-op at the moment
                update_ret = await hub.exec.vcenter.vm.update(
                    ctx,
                    **{
                        "resource_id": resource_id,
                        "name": name,
                    },
                )
                result["result"] = update_ret["result"]

                if result["result"]:
                    result["comment"].append(f"Updated 'vcenter.vm: {name}'")
                else:
                    result["comment"].append(update_ret["comment"])
    else:
        if ctx.test:
            result["new_state"] = hub.tool.vcenter.test_state_utils.generate_test_state(
                enforced_state={}, desired_state=desired_state
            )
            result["comment"] = (f"Would create vcenter.vm: {name}",)
            return result
        else:
            create_ret = await hub.exec.vcenter.vm.create(
                ctx,
                **{
                    "name": name,
                    "guest_os": guest_os,
                    "placement": placement,
                    "hardware_version": hardware_version,
                    "boot": boot,
                    "boot_devices": boot_devices,
                    "cpu": cpu,
                    "memory": memory,
                    "disks": disks,
                    "nics": nics,
                    "cdroms": cdroms,
                    "floppies": floppies,
                    "parallel_ports": parallel_ports,
                    "serial_ports": serial_ports,
                    "sata_adapters": sata_adapters,
                    "scsi_adapters": scsi_adapters,
                    "nvme_adapters": nvme_adapters,
                    "storage_policy": storage_policy,
                },
            )
            result["result"] = create_ret["result"]

            if result["result"]:
                result["comment"].append(f"Created 'vcenter.vm: {name}'")
                resource_id = create_ret["ret"]["resource_id"]
                # Safeguard for any future errors so that the resource_id is saved in the ESM
                result["new_state"] = dict(name=name, resource_id=resource_id)
            else:
                result["comment"].append(create_ret["comment"])

    if not result["result"]:
        # If there is any failure in create/update, it should reconcile.
        # The type of data is less important here to use default reconciliation
        # If there are no changes for 3 runs with rerun_data, then it will come out of execution
        result["rerun_data"] = dict(name=name, resource_id=resource_id)

    after = await hub.exec.vcenter.vm.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    result["new_state"] = after.ret
    return result


async def absent(
    hub,
    ctx,
    name: str,
    resource_id: str = None,
) -> Dict[str, Any]:
    """
    Deletes a virtual machine.
        if you do not have all of the privileges described as follows:
        -  The resource VirtualMachine referenced by the parameter vm requires VirtualMachine.Inventory.Delete.

    Args:
        name(str):
            Idem name of the resource.

        vm(str):
            Virtual machine identifier.
            The parameter must be an identifier for the resource type: VirtualMachine.

        resource_id(str, Optional):
            Vm unique ID. Defaults to None.

    Returns:
        Dict[str, Any]

    Example:
        .. code-block:: sls

            idem_test_vcenter.vm_is_absent:
              vcenter.vcenter.vm.absent:
                - name: string
                - resource_id: string


    """

    result = dict(
        comment=[], old_state={}, new_state={}, name=name, result=True, rerun_data=None
    )

    if not resource_id:
        resource_id = (ctx.old_state or {}).get("resource_id")

    if not resource_id:
        result["comment"].append(f"'vcenter.vm: {name}' already absent")
        return result

    before = await hub.exec.vcenter.vm.get(
        ctx,
        name=name,
        resource_id=resource_id,
    )

    if before["ret"]:
        if ctx.test:
            result["comment"] = f"Would delete vcenter.vm: {name}"
            return result

        delete_ret = await hub.exec.vcenter.vm.delete(
            ctx,
            name=name,
            resource_id=resource_id,
        )
        result["result"] = delete_ret["result"]

        if result["result"]:
            result["comment"].append(f"Deleted vcenter.vm: {name}")
        else:
            # If there is any failure in delete, it should reconcile.
            # The type of data is less important here to use default reconciliation
            # If there are no changes for 3 runs with rerun_data, then it will come out of execution
            result["rerun_data"] = resource_id
            result["comment"].append(delete_ret["result"])
    else:
        result["comment"].append(f"vcenter.vm: {name} already absent")
        return result

    result["old_state"] = before.ret
    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Describe the resource in a way that can be recreated/managed with the corresponding "present" function

    Returns information about at most 4000 visible (subject to permission checks) virtual machines in vCenter matching the VM.FilterSpec.

    Returns:
        Dict[str, Any]

    Example:

        .. code-block:: bash

            $ idem describe vcenter.vm
    """

    result = {}

    ret = await hub.exec.vcenter.vm.list(ctx)

    if not ret or not ret["result"]:
        hub.log.debug(f"Could not describe vcenter.vm {ret['comment']}")
        return result

    for resource in ret["ret"]:
        resource_id = resource.get("resource_id")
        result[resource_id] = {
            "vcenter.vm.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource.items()
            ]
        }
    return result

"""Exec module for managing Vms. """
from collections import OrderedDict
from dataclasses import field
from dataclasses import make_dataclass
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["soft_fail"]

__func_alias__ = {"list_": "list"}


async def get(hub, ctx, resource_id: str, name: str = None) -> Dict[str, Any]:
    """
    Returns information about a virtual machine.
        if you do not have all of the privileges described as follows:
        - The resource VirtualMachine referenced by the parameter vm requires System.Read.

    Args:
        resource_id(str):
            Virtual machine identifier.
            The parameter must be an identifier for the resource type: VirtualMachine.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcenter.vm.get
                - kwargs:
                    resource_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcenter.vm.get resource_id=value
    """

    result = dict(comment=[], ret=None, result=True)

    get = await hub.tool.vcenter.session.request(
        ctx,
        method="get",
        path="/api/vcenter/vm/{vm}".format(**{"vm": resource_id}),
        query_params={},
        data={},
        headers={},
    )

    if not get["result"]:
        # Send empty result for not found
        if get["status"] == 404:
            result["comment"].append(f"Get '{name}' result is empty")
            return result

        result["comment"].append(get["comment"])
        result["result"] = False
        return result

    # Case: Empty results
    if not get["ret"]:
        result["comment"].append(f"Get '{name}' result is empty")
        return result

    # Convert raw response into present format
    raw_resource = get["ret"]

    resource_in_present_format = {"name": name, "resource_id": resource_id}
    resource_parameters = OrderedDict(
        {
            "boot": "boot",
            "boot_devices": "boot_devices",
            "cdroms": "cdroms",
            "cpu": "cpu",
            "disks": "disks",
            "floppies": "floppies",
            "guest_os": "guest_OS",
            "hardware": "hardware",
            "identity": "identity",
            "instant_clone_frozen": "instant_clone_frozen",
            "memory": "memory",
            "name": "name",
            "nics": "nics",
            "nvme_adapters": "nvme_adapters",
            "parallel_ports": "parallel_ports",
            "power_state": "power_state",
            "sata_adapters": "sata_adapters",
            "scsi_adapters": "scsi_adapters",
            "serial_ports": "serial_ports",
        }
    )

    for parameter_raw, parameter_present in resource_parameters.items():
        if parameter_raw in raw_resource and raw_resource.get(parameter_raw):
            resource_in_present_format[parameter_present] = raw_resource.get(
                parameter_raw
            )

    result["ret"] = resource_in_present_format

    return result


async def list_(
    hub,
    ctx,
    name: str = None,
    vms: List[str] = None,
    names: List[str] = None,
    folders: List[str] = None,
    datacenters: List[str] = None,
    hosts: List[str] = None,
    clusters: List[str] = None,
    resource_pools: List[str] = None,
    power_states: List[str] = None,
) -> Dict[str, Any]:
    """
    Returns information about at most 4000 visible (subject to permission checks) virtual machines in vCenter matching the VM.FilterSpec.

    Args:
        name(str, Optional):
            Idem name of the resource. Defaults to None.

        vms(List[str], Optional):
            Identifiers of virtual machines that can match the filter.
            If unset or empty, virtual machines with any identifier match the filter.
            When clients pass a value of this structure as a parameter, the field must contain identifiers for the resource type: VirtualMachine. When operations return a value of this structure as a result, the field will contain identifiers for the resource type: VirtualMachine. Defaults to None.

        names(List[str], Optional):
            Names that virtual machines must have to match the filter (see VM.Info.name).
            If unset or empty, virtual machines with any name match the filter. Defaults to None.

        folders(List[str], Optional):
            Folders that must contain the virtual machine for the virtual machine to match the filter.
            If unset or empty, virtual machines in any folder match the filter.
            When clients pass a value of this structure as a parameter, the field must contain identifiers for the resource type: Folder. When operations return a value of this structure as a result, the field will contain identifiers for the resource type: Folder. Defaults to None.

        datacenters(List[str], Optional):
            Datacenters that must contain the virtual machine for the virtual machine to match the filter.
            If unset or empty, virtual machines in any datacenter match the filter.
            When clients pass a value of this structure as a parameter, the field must contain identifiers for the resource type: Datacenter. When operations return a value of this structure as a result, the field will contain identifiers for the resource type: Datacenter. Defaults to None.

        hosts(List[str], Optional):
            Hosts that must contain the virtual machine for the virtual machine to match the filter.
            If unset or empty, virtual machines on any host match the filter.
            When clients pass a value of this structure as a parameter, the field must contain identifiers for the resource type: HostSystem. When operations return a value of this structure as a result, the field will contain identifiers for the resource type: HostSystem. Defaults to None.

        clusters(List[str], Optional):
            Clusters that must contain the virtual machine for the virtual machine to match the filter.
            If unset or empty, virtual machines in any cluster match the filter.
            When clients pass a value of this structure as a parameter, the field must contain identifiers for the resource type: ClusterComputeResource. When operations return a value of this structure as a result, the field will contain identifiers for the resource type: ClusterComputeResource. Defaults to None.

        resource_pools(List[str], Optional):
            Resource pools that must contain the virtual machine for the virtual machine to match the filter.
            If unset or empty, virtual machines in any resource pool match the filter.
            When clients pass a value of this structure as a parameter, the field must contain identifiers for the resource type: ResourcePool. When operations return a value of this structure as a result, the field will contain identifiers for the resource type: ResourcePool. Defaults to None.

        power_states(List[str], Optional):
            Power states that a virtual machine must be in to match the filter (see Power.Info.state.
            If unset or empty, virtual machines in any power state match the filter. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:

        Resource State:

        .. code-block:: sls

            unmanaged_resources:
              exec.run:
                - path: vcenter.vm.list
                - kwargs:


        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcenter.vm.list

        Describe call from the CLI:

        .. code-block:: bash

            $ idem describe vcenter.vm

    """

    result = dict(comment=[], ret=[], result=True)

    list = await hub.tool.vcenter.session.request(
        ctx,
        method="get",
        path="/api/vcenter/vm",
        query_params={
            "vms": vms,
            "names": names,
            "folders": folders,
            "datacenters": datacenters,
            "hosts": hosts,
            "clusters": clusters,
            "resource_pools": resource_pools,
            "power_states": power_states,
        },
        data={},
        headers={},
    )

    if not list["result"]:
        result["comment"].append(list["comment"])
        result["result"] = False
        return result

    for resource in list["ret"]:
        if resource:
            # Get full VM info
            vm_info = await hub.exec.vcenter.vm.get(ctx, resource_id=resource.get("vm"))

            # A failed GET or 404 is going to skip the machine
            if not vm_info["result"] or not vm_info["ret"]:
                hub.log.warning(
                    f"Failed to get Virtual Machine information for {resource.get('name')}: {vm_info['comment']}"
                )
                continue

            result["ret"].append(vm_info["ret"])

    return result


async def create(
    hub,
    ctx,
    guest_os: str,
    name: str = None,
    placement: make_dataclass(
        "placement",
        [
            ("folder", str, field(default=None)),
            ("resource_pool", str, field(default=None)),
            ("host", str, field(default=None)),
            ("cluster", str, field(default=None)),
            ("datastore", str, field(default=None)),
        ],
    ) = None,
    hardware_version: str = None,
    boot: make_dataclass(
        "boot",
        [
            ("type", str, field(default=None)),
            ("efi_legacy_boot", bool, field(default=None)),
            ("network_protocol", str, field(default=None)),
            ("delay", int, field(default=None)),
            ("retry", bool, field(default=None)),
            ("retry_delay", int, field(default=None)),
            ("enter_setup_mode", bool, field(default=None)),
        ],
    ) = None,
    boot_devices: List[make_dataclass("boot_devices", [("type", str)])] = None,
    cpu: make_dataclass(
        "cpu",
        [
            ("count", int, field(default=None)),
            ("cores_per_socket", int, field(default=None)),
            ("hot_add_enabled", bool, field(default=None)),
            ("hot_remove_enabled", bool, field(default=None)),
        ],
    ) = None,
    memory: make_dataclass(
        "memory",
        [
            ("size_mi_b", int, field(default=None)),
            ("hot_add_enabled", bool, field(default=None)),
        ],
    ) = None,
    disks: List[
        make_dataclass(
            "disks",
            [
                ("type", str, field(default=None)),
                (
                    "ide",
                    make_dataclass(
                        "ide",
                        [
                            ("primary", bool, field(default=None)),
                            ("master", bool, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                (
                    "scsi",
                    make_dataclass(
                        "scsi", [("bus", int), ("unit", int, field(default=None))]
                    ),
                    field(default=None),
                ),
                (
                    "sata",
                    make_dataclass(
                        "sata", [("bus", int), ("unit", int, field(default=None))]
                    ),
                    field(default=None),
                ),
                (
                    "nvme",
                    make_dataclass(
                        "nvme", [("bus", int), ("unit", int, field(default=None))]
                    ),
                    field(default=None),
                ),
                (
                    "backing",
                    make_dataclass(
                        "backing",
                        [("type", str), ("vmdk_file", str, field(default=None))],
                    ),
                    field(default=None),
                ),
                (
                    "new_vmdk",
                    make_dataclass(
                        "new_vmdk",
                        [
                            ("name", str, field(default=None)),
                            ("capacity", int, field(default=None)),
                            (
                                "storage_policy",
                                make_dataclass("storage_policy", [("policy", str)]),
                                field(default=None),
                            ),
                        ],
                    ),
                    field(default=None),
                ),
            ],
        )
    ] = None,
    nics: List[
        make_dataclass(
            "nics",
            [
                ("type", str, field(default=None)),
                ("upt_compatibility_enabled", bool, field(default=None)),
                ("mac_type", str, field(default=None)),
                ("mac_address", str, field(default=None)),
                ("pci_slot_number", int, field(default=None)),
                ("wake_on_lan_enabled", bool, field(default=None)),
                (
                    "backing",
                    make_dataclass(
                        "backing",
                        [
                            ("type", str),
                            ("network", str, field(default=None)),
                            ("distributed_port", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("start_connected", bool, field(default=None)),
                ("allow_guest_control", bool, field(default=None)),
            ],
        )
    ] = None,
    cdroms: List[
        make_dataclass(
            "cdroms",
            [
                ("type", str, field(default=None)),
                (
                    "ide",
                    make_dataclass(
                        "ide",
                        [
                            ("primary", bool, field(default=None)),
                            ("master", bool, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                (
                    "sata",
                    make_dataclass(
                        "sata", [("bus", int), ("unit", int, field(default=None))]
                    ),
                    field(default=None),
                ),
                (
                    "backing",
                    make_dataclass(
                        "backing",
                        [
                            ("type", str),
                            ("iso_file", str, field(default=None)),
                            ("host_device", str, field(default=None)),
                            ("device_access_type", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("start_connected", bool, field(default=None)),
                ("allow_guest_control", bool, field(default=None)),
            ],
        )
    ] = None,
    floppies: List[
        make_dataclass(
            "floppies",
            [
                (
                    "backing",
                    make_dataclass(
                        "backing",
                        [
                            ("type", str),
                            ("image_file", str, field(default=None)),
                            ("host_device", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("start_connected", bool, field(default=None)),
                ("allow_guest_control", bool, field(default=None)),
            ],
        )
    ] = None,
    parallel_ports: List[
        make_dataclass(
            "parallel_ports",
            [
                (
                    "backing",
                    make_dataclass(
                        "backing",
                        [
                            ("type", str),
                            ("file", str, field(default=None)),
                            ("host_device", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("start_connected", bool, field(default=None)),
                ("allow_guest_control", bool, field(default=None)),
            ],
        )
    ] = None,
    serial_ports: List[
        make_dataclass(
            "serial_ports",
            [
                ("yield_on_poll", bool, field(default=None)),
                (
                    "backing",
                    make_dataclass(
                        "backing",
                        [
                            ("type", str),
                            ("file", str, field(default=None)),
                            ("host_device", str, field(default=None)),
                            ("pipe", str, field(default=None)),
                            ("no_rx_loss", bool, field(default=None)),
                            ("network_location", str, field(default=None)),
                            ("proxy", str, field(default=None)),
                        ],
                    ),
                    field(default=None),
                ),
                ("start_connected", bool, field(default=None)),
                ("allow_guest_control", bool, field(default=None)),
            ],
        )
    ] = None,
    sata_adapters: List[
        make_dataclass(
            "sata_adapters",
            [
                ("type", str, field(default=None)),
                ("bus", int, field(default=None)),
                ("pci_slot_number", int, field(default=None)),
            ],
        )
    ] = None,
    scsi_adapters: List[
        make_dataclass(
            "scsi_adapters",
            [
                ("type", str, field(default=None)),
                ("bus", int, field(default=None)),
                ("pci_slot_number", int, field(default=None)),
                ("sharing", str, field(default=None)),
            ],
        )
    ] = None,
    nvme_adapters: List[
        make_dataclass(
            "nvme_adapters",
            [
                ("bus", int, field(default=None)),
                ("pci_slot_number", int, field(default=None)),
            ],
        )
    ] = None,
    storage_policy: make_dataclass("storage_policy", [("policy", str)]) = None,
) -> Dict[str, Any]:
    """
    Creates a virtual machine.
        if you do not have all of the privileges described as follows:
        -  The resource Folder referenced by the attribute VM.InventoryPlacementSpec.folder requires VirtualMachine.Inventory.Create.
        -  The resource ResourcePool referenced by the attribute VM.ComputePlacementSpec.resource-pool requires Resource.AssignVMToPool.
        -  The resource Datastore referenced by the attribute VM.StoragePlacementSpec.datastore requires Datastore.AllocateSpace.
        -  The resource Network referenced by the attribute Ethernet.BackingSpec.network requires Network.Assign.



    Args:
        guest_os(str):
            The GuestOS enumerated type defines the valid guest operating system types used for configuring a virtual machine.

        resource_id(str, Optional):
            Vm unique ID. Defaults to None.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

        placement(dict[str, Any], Optional):
            placement. Defaults to None.

            * folder (str, Optional):
                Virtual machine folder into which the virtual machine should be placed.
                This field is currently required. In the future, if this field is unset, the system will attempt to choose a suitable folder for the virtual machine; if a folder cannot be chosen, the virtual machine creation operation will fail.
                When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: Folder. When operations return a value of this structure as a result, the field will be an identifier for the resource type: Folder. Defaults to None.

            * resource_pool (str, Optional):
                Resource pool into which the virtual machine should be placed.
                This field is currently required if both VM.ComputePlacementSpec.host and VM.ComputePlacementSpec.cluster are unset. In the future, if this field is unset, the system will attempt to choose a suitable resource pool for the virtual machine; if a resource pool cannot be chosen, the virtual machine creation operation will fail.
                When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: ResourcePool. When operations return a value of this structure as a result, the field will be an identifier for the resource type: ResourcePool. Defaults to None.

            * host (str, Optional):
                Host onto which the virtual machine should be placed.
                 If VM.ComputePlacementSpec.host and VM.ComputePlacementSpec.resource-pool are both specified, VM.ComputePlacementSpec.resource-pool must belong to VM.ComputePlacementSpec.host.

                 If VM.ComputePlacementSpec.host and VM.ComputePlacementSpec.cluster are both specified, VM.ComputePlacementSpec.host must be a member of VM.ComputePlacementSpec.cluster.

                This field may be unset if VM.ComputePlacementSpec.resource-pool or VM.ComputePlacementSpec.cluster is specified. If unset, the system will attempt to choose a suitable host for the virtual machine; if a host cannot be chosen, the virtual machine creation operation will fail.
                When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: HostSystem. When operations return a value of this structure as a result, the field will be an identifier for the resource type: HostSystem. Defaults to None.

            * cluster (str, Optional):
                Cluster into which the virtual machine should be placed.
                 If VM.ComputePlacementSpec.cluster and VM.ComputePlacementSpec.resource-pool are both specified, VM.ComputePlacementSpec.resource-pool must belong to VM.ComputePlacementSpec.cluster.

                 If VM.ComputePlacementSpec.cluster and VM.ComputePlacementSpec.host are both specified, VM.ComputePlacementSpec.host must be a member of VM.ComputePlacementSpec.cluster.

                If VM.ComputePlacementSpec.resource-pool or VM.ComputePlacementSpec.host is specified, it is recommended that this field be unset.
                When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: ClusterComputeResource. When operations return a value of this structure as a result, the field will be an identifier for the resource type: ClusterComputeResource. Defaults to None.

            * datastore (str, Optional):
                Datastore on which the virtual machine's configuration state should be stored. This datastore will also be used for any virtual disks that are created as part of the virtual machine creation operation.
                This field is currently required. In the future, if this field is unset, the system will attempt to choose suitable storage for the virtual machine; if storage cannot be chosen, the virtual machine creation operation will fail.
                When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: Datastore. When operations return a value of this structure as a result, the field will be an identifier for the resource type: Datastore. Defaults to None.

        hardware_version(str, Optional):
            The Hardware.Version enumerated type defines the valid virtual hardware versions for a virtual machine. See https://kb.vmware.com/s/article/1003746 (Virtual machine hardware versions (1003746)). Defaults to None.

        boot(dict[str, Any], Optional):
            boot. Defaults to None.

            * type (str, Optional):
                The Boot.Type enumerated type defines the valid firmware types for a virtual machine. Defaults to None.

            * efi_legacy_boot (bool, Optional):
                Flag indicating whether to use EFI legacy boot mode.
                If unset, defaults to value that is recommended for the guest OS and is supported for the virtual hardware version. Defaults to None.

            * network_protocol (str, Optional):
                The Boot.NetworkProtocol enumerated type defines the valid network boot protocols supported when booting a virtual machine with EFI firmware over the network. Defaults to None.

            * delay (int, Optional):
                Delay in milliseconds before beginning the firmware boot process when the virtual machine is powered on. This delay may be used to provide a time window for users to connect to the virtual machine console and enter BIOS setup mode.
                If unset, default value is 0. Defaults to None.

            * retry (bool, Optional):
                Flag indicating whether the virtual machine should automatically retry the boot process after a failure.
                If unset, default value is false. Defaults to None.

            * retry_delay (int, Optional):
                Delay in milliseconds before retrying the boot process after a failure; applicable only when Boot.Info.retry is true.
                If unset, default value is 10000. Defaults to None.

            * enter_setup_mode (bool, Optional):
                Flag indicating whether the firmware boot process should automatically enter setup mode the next time the virtual machine boots. Note that this flag will automatically be reset to false once the virtual machine enters setup mode.
                If unset, the value is unchanged. Defaults to None.

        boot_devices(List[dict[str, Any]], Optional):
            Boot device configuration.
            If unset, a server-specific boot sequence will be used. Defaults to None.

            * type (str):
                The Device.Type enumerated type defines the valid device types that may be used as bootable devices.

        cpu(dict[str, Any], Optional):
            cpu. Defaults to None.

            * count (int, Optional):
                New number of CPU cores. The number of CPU cores in the virtual machine must be a multiple of the number of cores per socket.
                 The supported range of CPU counts is constrained by the configured guest operating system and virtual hardware version of the virtual machine.

                 If the virtual machine is running, the number of CPU cores may only be increased if Cpu.Info.hot-add-enabled is true, and may only be decreased if Cpu.Info.hot-remove-enabled is true.

                If unset, the value is unchanged. Defaults to None.

            * cores_per_socket (int, Optional):
                New number of CPU cores per socket. The number of CPU cores in the virtual machine must be a multiple of the number of cores per socket.
                If unset, the value is unchanged. Defaults to None.

            * hot_add_enabled (bool, Optional):
                Flag indicating whether adding CPUs while the virtual machine is running is enabled.
                 This field may only be modified if the virtual machine is powered off.

                If unset, the value is unchanged. Defaults to None.

            * hot_remove_enabled (bool, Optional):
                Flag indicating whether removing CPUs while the virtual machine is running is enabled.
                 This field may only be modified if the virtual machine is powered off.

                If unset, the value is unchanged. Defaults to None.

        memory(dict[str, Any], Optional):
            memory. Defaults to None.

            * size_mi_b (int, Optional):
                New memory size in mebibytes.
                 The supported range of memory sizes is constrained by the configured guest operating system and virtual hardware version of the virtual machine.

                 If the virtual machine is running, this value may only be changed if Memory.Info.hot-add-enabled is true, and the new memory size must satisfy the constraints specified by Memory.Info.hot-add-increment-size-mib and Memory.Info.hot-add-limit-mib.

                If unset, the value is unchanged. Defaults to None.

            * hot_add_enabled (bool, Optional):
                Flag indicating whether adding memory while the virtual machine is running should be enabled.
                 Some guest operating systems may consume more resources or perform less efficiently when they run on hardware that supports adding memory while the machine is running.

                 This field may only be modified if the virtual machine is not powered on.

                If unset, the value is unchanged. Defaults to None.

        disks(List[dict[str, Any]], Optional):
            List of disks.
            If unset, a single blank virtual disk of a guest-specific size will be created on the same storage as the virtual machine configuration, and will use a guest-specific host bus adapter type. If the guest-specific size is 0, no virtual disk will be created. Defaults to None.

            * type (str, Optional):
                The Disk.HostBusAdapterType enumerated type defines the valid types of host bus adapters that may be used for attaching a virtual storage device to a virtual machine. Defaults to None.

            * ide (dict[str, Any], Optional):
                ide. Defaults to None.

                * primary (bool, Optional):
                    Flag specifying whether the device should be attached to the primary or secondary IDE adapter of the virtual machine.
                    If unset, the server will choose a adapter with an available connection. If no IDE connections are available, the request will be rejected. Defaults to None.

                * master (bool, Optional):
                    Flag specifying whether the device should be the master or slave device on the IDE adapter.
                    If unset, the server will choose an available connection type. If no IDE connections are available, the request will be rejected. Defaults to None.

            * scsi (dict[str, Any], Optional):
                scsi. Defaults to None.

                * bus (int):
                    Bus number of the adapter to which the device should be attached.

                * unit (int, Optional):
                    Unit number of the device.
                    If unset, the server will choose an available unit number on the specified adapter. If there are no available connections on the adapter, the request will be rejected. Defaults to None.

            * sata (dict[str, Any], Optional):
                sata. Defaults to None.

                * bus (int):
                    Bus number of the adapter to which the device should be attached.

                * unit (int, Optional):
                    Unit number of the device.
                    If unset, the server will choose an available unit number on the specified adapter. If there are no available connections on the adapter, the request will be rejected. Defaults to None.

            * nvme (dict[str, Any], Optional):
                nvme. Defaults to None.

                * bus (int):
                    Bus number of the adapter to which the device should be attached.

                * unit (int, Optional):
                    Unit number of the device.
                    If unset, the server will choose an available unit number on the specified adapter. If there are no available connections on the adapter, the request will be rejected. Defaults to None.

            * backing (dict[str, Any], Optional):
                backing. Defaults to None.

                * type (str):
                    The Disk.BackingType enumerated type defines the valid backing types for a virtual disk.

                * vmdk_file (str, Optional):
                    Path of the VMDK file backing the virtual disk.
                    This field is optional and it is only relevant when the value of Disk.BackingSpec.type is VMDK_FILE. Defaults to None.

            * new_vmdk (dict[str, Any], Optional):
                new_vmdk. Defaults to None.

                * name (str, Optional):
                    Base name of the VMDK file. The name should not include the '.vmdk' file extension.
                    If unset, a name (derived from the name of the virtual machine) will be chosen by the server. Defaults to None.

                * capacity (int, Optional):
                    Capacity of the virtual disk backing in bytes.
                    If unset, defaults to a guest-specific capacity. Defaults to None.

                * storage_policy (dict[str, Any], Optional):
                    storage_policy. Defaults to None.

                    * policy (str):
                        Identifier of the storage policy which should be associated with the VMDK file.
                        When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: vcenter.StoragePolicy. When operations return a value of this structure as a result, the field will be an identifier for the resource type: vcenter.StoragePolicy.

        nics(List[dict[str, Any]], Optional):
            List of Ethernet adapters.
            If unset, no Ethernet adapters will be created. Defaults to None.

            * type (str, Optional):
                The Ethernet.EmulationType enumerated type defines the valid emulation types for a virtual Ethernet adapter. Defaults to None.

            * upt_compatibility_enabled (bool, Optional):
                Flag indicating whether Universal Pass-Through (UPT) compatibility is enabled on this virtual Ethernet adapter.
                If unset, defaults to false. Defaults to None.

            * mac_type (str, Optional):
                The Ethernet.MacAddressType enumerated type defines the valid MAC address origins for a virtual Ethernet adapter. Defaults to None.

            * mac_address (str, Optional):
                MAC address.
                Workaround for PR1459647. Defaults to None.

            * pci_slot_number (int, Optional):
                Address of the virtual Ethernet adapter on the PCI bus. If the PCI address is invalid, the server will change when it the VM is started or as the device is hot added.
                If unset, the server will choose an available address when the virtual machine is powered on. Defaults to None.

            * wake_on_lan_enabled (bool, Optional):
                Flag indicating whether wake-on-LAN is enabled on this virtual Ethernet adapter.
                Defaults to false if unset. Defaults to None.

            * backing (dict[str, Any], Optional):
                backing. Defaults to None.

                * type (str):
                    The Ethernet.BackingType enumerated type defines the valid backing types for a virtual Ethernet adapter.

                * network (str, Optional):
                    Identifier of the network that backs the virtual Ethernet adapter.
                    This field is optional and it is only relevant when the value of Ethernet.BackingSpec.type is one of STANDARD_PORTGROUP, DISTRIBUTED_PORTGROUP, or OPAQUE_NETWORK.
                    When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: Network. When operations return a value of this structure as a result, the field will be an identifier for the resource type: Network. Defaults to None.

                * distributed_port (str, Optional):
                    Key of the distributed virtual port that backs the virtual Ethernet adapter. Depending on the type of the Portgroup, the port may be specified using this field. If the portgroup type is early-binding (also known as static), a port is assigned when the Ethernet adapter is configured to use the port. The port may be either automatically or specifically assigned based on the value of this field. If the portgroup type is ephemeral, the port is created and assigned to a virtual machine when it is powered on and the Ethernet adapter is connected. This field cannot be specified as no free ports exist before use.
                    May be used to specify a port when the network specified on the Ethernet.BackingSpec.network field is a static or early binding distributed portgroup. If unset, the port will be automatically assigned to the Ethernet adapter based on the policy embodied by the portgroup type. Defaults to None.

            * start_connected (bool, Optional):
                Flag indicating whether the virtual device should be connected whenever the virtual machine is powered on.
                Defaults to false if unset. Defaults to None.

            * allow_guest_control (bool, Optional):
                Flag indicating whether the guest can connect and disconnect the device.
                Defaults to false if unset. Defaults to None.

        cdroms(List[dict[str, Any]], Optional):
            List of CD-ROMs.
            If unset, no CD-ROM devices will be created. Defaults to None.

            * type (str, Optional):
                The Cdrom.HostBusAdapterType enumerated type defines the valid types of host bus adapters that may be used for attaching a Cdrom to a virtual machine. Defaults to None.

            * ide (dict[str, Any], Optional):
                ide. Defaults to None.

                * primary (bool, Optional):
                    Flag specifying whether the device should be attached to the primary or secondary IDE adapter of the virtual machine.
                    If unset, the server will choose a adapter with an available connection. If no IDE connections are available, the request will be rejected. Defaults to None.

                * master (bool, Optional):
                    Flag specifying whether the device should be the master or slave device on the IDE adapter.
                    If unset, the server will choose an available connection type. If no IDE connections are available, the request will be rejected. Defaults to None.

            * sata (dict[str, Any], Optional):
                sata. Defaults to None.

                * bus (int):
                    Bus number of the adapter to which the device should be attached.

                * unit (int, Optional):
                    Unit number of the device.
                    If unset, the server will choose an available unit number on the specified adapter. If there are no available connections on the adapter, the request will be rejected. Defaults to None.

            * backing (dict[str, Any], Optional):
                backing. Defaults to None.

                * type (str):
                    The Cdrom.BackingType enumerated type defines the valid backing types for a virtual CD-ROM device.

                * iso_file (str, Optional):
                    Path of the image file that should be used as the virtual CD-ROM device backing.
                    This field is optional and it is only relevant when the value of Cdrom.BackingSpec.type is ISO_FILE. Defaults to None.

                * host_device (str, Optional):
                    Name of the device that should be used as the virtual CD-ROM device backing.
                    If unset, the virtual CD-ROM device will be configured to automatically detect a suitable host device. Defaults to None.

                * device_access_type (str, Optional):
                    The Cdrom.DeviceAccessType enumerated type defines the valid device access types for a physical device packing of a virtual CD-ROM device. Defaults to None.

            * start_connected (bool, Optional):
                Flag indicating whether the virtual device should be connected whenever the virtual machine is powered on.
                Defaults to false if unset. Defaults to None.

            * allow_guest_control (bool, Optional):
                Flag indicating whether the guest can connect and disconnect the device.
                Defaults to false if unset. Defaults to None.

        floppies(List[dict[str, Any]], Optional):
            List of floppy drives.
            If unset, no floppy drives will be created. Defaults to None.

            * backing (dict[str, Any], Optional):
                backing. Defaults to None.

                * type (str):
                    The Floppy.BackingType enumerated type defines the valid backing types for a virtual floppy drive.

                * image_file (str, Optional):
                    Path of the image file that should be used as the virtual floppy drive backing.
                    This field is optional and it is only relevant when the value of Floppy.BackingSpec.type is IMAGE_FILE. Defaults to None.

                * host_device (str, Optional):
                    Name of the device that should be used as the virtual floppy drive backing.
                    If unset, the virtual floppy drive will be configured to automatically detect a suitable host device. Defaults to None.

            * start_connected (bool, Optional):
                Flag indicating whether the virtual device should be connected whenever the virtual machine is powered on.
                Defaults to false if unset. Defaults to None.

            * allow_guest_control (bool, Optional):
                Flag indicating whether the guest can connect and disconnect the device.
                Defaults to false if unset. Defaults to None.

        parallel_ports(List[dict[str, Any]], Optional):
            List of parallel ports.
            If unset, no parallel ports will be created. Defaults to None.

            * backing (dict[str, Any], Optional):
                backing. Defaults to None.

                * type (str):
                    The Parallel.BackingType enumerated type defines the valid backing types for a virtual parallel port.

                * file (str, Optional):
                    Path of the file that should be used as the virtual parallel port backing.
                    This field is optional and it is only relevant when the value of Parallel.BackingSpec.type is FILE. Defaults to None.

                * host_device (str, Optional):
                    Name of the device that should be used as the virtual parallel port backing.
                    If unset, the virtual parallel port will be configured to automatically detect a suitable host device. Defaults to None.

            * start_connected (bool, Optional):
                Flag indicating whether the virtual device should be connected whenever the virtual machine is powered on.
                Defaults to false if unset. Defaults to None.

            * allow_guest_control (bool, Optional):
                Flag indicating whether the guest can connect and disconnect the device.
                Defaults to false if unset. Defaults to None.

        serial_ports(List[dict[str, Any]], Optional):
            List of serial ports.
            If unset, no serial ports will be created. Defaults to None.

            * yield_on_poll (bool, Optional):
                CPU yield behavior. If set to true, the virtual machine will periodically relinquish the processor if its sole task is polling the virtual serial port. The amount of time it takes to regain the processor will depend on the degree of other virtual machine activity on the host.
                If unset, defaults to false. Defaults to None.

            * backing (dict[str, Any], Optional):
                backing. Defaults to None.

                * type (str):
                    The Serial.BackingType enumerated type defines the valid backing types for a virtual serial port.

                * file (str, Optional):
                    Path of the file backing the virtual serial port.
                    This field is optional and it is only relevant when the value of Serial.BackingSpec.type is FILE. Defaults to None.

                * host_device (str, Optional):
                    Name of the device backing the virtual serial port.


                    If unset, the virtual serial port will be configured to automatically detect a suitable host device. Defaults to None.

                * pipe (str, Optional):
                    Name of the pipe backing the virtual serial port.
                    This field is optional and it is only relevant when the value of Serial.BackingSpec.type is one of PIPE_SERVER or PIPE_CLIENT. Defaults to None.

                * no_rx_loss (bool, Optional):
                    Flag that enables optimized data transfer over the pipe. When the value is true, the host buffers data to prevent data overrun. This allows the virtual machine to read all of the data transferred over the pipe with no data loss.
                    If unset, defaults to false. Defaults to None.

                * network_location (str, Optional):
                    URI specifying the location of the network service backing the virtual serial port.
                       - If Serial.BackingSpec.type is NETWORK_SERVER, this field is the location used by clients to connect to this server. The hostname part of the URI should either be empty or should specify the address of the host on which the virtual machine is running.
                       - If Serial.BackingSpec.type is NETWORK_CLIENT, this field is the location used by the virtual machine to connect to the remote server.

                    This field is optional and it is only relevant when the value of Serial.BackingSpec.type is one of NETWORK_SERVER or NETWORK_CLIENT. Defaults to None.

                * proxy (str, Optional):
                    Proxy service that provides network access to the network backing. If set, the virtual machine initiates a connection with the proxy service and forwards the traffic to the proxy.
                    If unset, no proxy service should be used. Defaults to None.

            * start_connected (bool, Optional):
                Flag indicating whether the virtual device should be connected whenever the virtual machine is powered on.
                Defaults to false if unset. Defaults to None.

            * allow_guest_control (bool, Optional):
                Flag indicating whether the guest can connect and disconnect the device.
                Defaults to false if unset. Defaults to None.

        sata_adapters(List[dict[str, Any]], Optional):
            List of SATA adapters.
            If unset, any adapters necessary to connect the virtual machine's storage devices will be created; this includes any devices that explicitly specify a SATA host bus adapter, as well as any devices that do not specify a host bus adapter if the guest's preferred adapter type is SATA. Defaults to None.

            * type (str, Optional):
                The Sata.Type enumerated type defines the valid emulation types for a virtual SATA adapter. Defaults to None.

            * bus (int, Optional):
                SATA bus number.
                If unset, the server will choose an available bus number; if none is available, the request will fail. Defaults to None.

            * pci_slot_number (int, Optional):
                Address of the SATA adapter on the PCI bus.
                If unset, the server will choose an available address when the virtual machine is powered on. Defaults to None.

        scsi_adapters(List[dict[str, Any]], Optional):
            List of SCSI adapters.
            If unset, any adapters necessary to connect the virtual machine's storage devices will be created; this includes any devices that explicitly specify a SCSI host bus adapter, as well as any devices that do not specify a host bus adapter if the guest's preferred adapter type is SCSI. The type of the SCSI adapter will be a guest-specific default type. Defaults to None.

            * type (str, Optional):
                The Scsi.Type enumerated type defines the valid emulation types for a virtual SCSI adapter. Defaults to None.

            * bus (int, Optional):
                SCSI bus number.
                If unset, the server will choose an available bus number; if none is available, the request will fail. Defaults to None.

            * pci_slot_number (int, Optional):
                Address of the SCSI adapter on the PCI bus. If the PCI address is invalid, the server will change it when the VM is started or as the device is hot added.
                If unset, the server will choose an available address when the virtual machine is powered on. Defaults to None.

            * sharing (str, Optional):
                The Scsi.Sharing enumerated type defines the valid bus sharing modes for a virtual SCSI adapter. Defaults to None.

        nvme_adapters(List[dict[str, Any]], Optional):
            List of NVMe adapters.
            If unset, any adapters necessary to connect the virtual machine's storage devices will be created; this includes any devices that explicitly specify a NVMe host bus adapter, as well as any devices that do not specify a host bus adapter if the guest's preferred adapter type is NVMe. Defaults to None.

            * bus (int, Optional):
                NVMe bus number.
                If unset, the server will choose an available bus number; if none is available, the request will fail. Defaults to None.

            * pci_slot_number (int, Optional):
                Address of the NVMe adapter on the PCI bus.
                If unset, the server will choose an available address when the virtual machine is powered on. Defaults to None.

        storage_policy(dict[str, Any], Optional):
            storage_policy. Defaults to None.

            * policy (str):
                Identifier of the storage policy which should be associated with the virtual machine.
                When clients pass a value of this structure as a parameter, the field must be an identifier for the resource type: vcenter.StoragePolicy. When operations return a value of this structure as a result, the field will be an identifier for the resource type: vcenter.StoragePolicy.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            resource_is_present:
              vcenter.vm.present:
                - guest_os: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcenter.vm.create guest_os=value
    """

    result = dict(comment=[], ret=[], result=True)

    desired_state = {
        k: v
        for k, v in locals().items()
        if k not in ("hub", "ctx", "result") and v is not None
    }

    resource_to_raw_input_mapping = {
        "guest_os": "guest_OS",
        "name": "name",
        "placement": "placement",
        "hardware_version": "hardware_version",
        "boot": "boot",
        "boot_devices": "boot_devices",
        "cpu": "cpu",
        "memory": "memory",
        "disks": "disks",
        "nics": "nics",
        "cdroms": "cdroms",
        "floppies": "floppies",
        "parallel_ports": "parallel_ports",
        "serial_ports": "serial_ports",
        "sata_adapters": "sata_adapters",
        "scsi_adapters": "scsi_adapters",
        "nvme_adapters": "nvme_adapters",
        "storage_policy": "storage_policy",
    }

    payload = {}
    for key, value in desired_state.items():
        if key in resource_to_raw_input_mapping.keys() and value is not None:
            payload[resource_to_raw_input_mapping[key]] = value

    create = await hub.tool.vcenter.session.request(
        ctx,
        method="post",
        path="/api/vcenter/vm",
        query_params={},
        data=payload,
        headers={},
    )

    if not create["result"]:
        result["comment"].append(create["comment"])
        result["result"] = False
        return result

    result["comment"].append(
        f"Created vcenter.vm '{name}'",
    )

    result["ret"] = {
        "name": name,
        # It returns machine's identifier
        "resource_id": create["ret"],
    }
    return result


async def update(hub, ctx, name: str, resource_id: str) -> Dict[str, Any]:
    """
    It is a no-op, returns the machine as is

    Args:
        name(str, Optional):
            Idem name of the resource. Defaults to None.

        resource_id(str):
            Virtual machine identifier.
            The parameter must be an identifier for the resource type: VirtualMachine.

    Returns:
        Dict[str, Any]

    Examples:
        Using in a state:

        .. code-block:: sls

            unmanaged_resource:
              exec.update:
                - path: vcenter.vm.update
                - kwargs:
                    name: value
                    resource_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcenter.vm.update name=value resource_id=value
    """

    result = dict(comment=[], ret=[], result=True)

    result["ret"] = {"name": name, "resource_id": resource_id}
    result["comment"].append(
        f"Updated vcenter.vm '{name}'",
    )

    return result


async def delete(hub, ctx, resource_id: str, name: str = None) -> Dict[str, Any]:
    """
    Deletes a virtual machine.
        if you do not have all of the privileges described as follows:
        -  The resource VirtualMachine referenced by the parameter vm requires VirtualMachine.Inventory.Delete.

    Args:
        resource_id(str):
            Virtual machine identifier.
            The parameter must be an identifier for the resource type: VirtualMachine.

        name(str, Optional):
            Idem name of the resource. Defaults to None.

    Returns:
        Dict[str, Any]

    Examples:
        Resource State:

        .. code-block:: sls

            unmanaged_resource:
              exec.run:
                - path: vcenter.vm.delete
                - kwargs:
                    resource_id: value

        Exec call from the CLI:

        .. code-block:: bash

            idem exec vcenter.vm.delete resource_id=value
    """

    result = dict(comment=[], ret=[], result=True)

    delete = await hub.tool.vcenter.session.request(
        ctx,
        method="delete",
        path="/api/vcenter/vm/{vm}".format(**{"vm": resource_id}),
        query_params={},
        data={},
        headers={},
    )

    if not delete["result"]:
        result["comment"].append(delete["comment"])
        result["result"] = False
        return result

    result["comment"].append(f"Deleted '{name}'")
    return result

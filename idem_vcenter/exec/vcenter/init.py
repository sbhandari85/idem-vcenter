def __init__(hub):
    # This enables acct profiles that begin with "vcenter" for vcenter modules
    hub.exec.vcenter.ACCT = ["vcenter"]

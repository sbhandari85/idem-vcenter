import base64
from typing import Any
from typing import Dict

from dict_tools.data import NamespaceDict


async def gather(hub, profiles) -> Dict[str, Any]:
    """
    Generate token with basic auth

    Example:
    .. code-block:: yaml

        vcenter:
          profile_name:
            username: my_user
            password: my_password
            endpoint_url: https://vcenter.com
    """
    sub_profiles = {}
    for (
        profile,
        ctx,
    ) in profiles.get("vcenter", {}).items():
        endpoint_url = ctx.get("endpoint_url")

        creds = f"{ctx.get('username')}:{ctx.get('password')}"
        temp_ctx = NamespaceDict(
            acct={
                "endpoint_url": endpoint_url,
                "headers": {
                    "Authorization": f"Basic {base64.b64encode(creds.encode('utf-8')).decode('ascii')}"
                },
            }
        )

        ret = await hub.tool.vcenter.session.request(
            temp_ctx,
            method="post",
            path="/api/session".format(**{}),
            data={},
        )

        if not ret["result"]:
            error = f"Unable to authenticate: {ret.get('comment', '')}"
            hub.log.error(error)
            raise ConnectionError(error)

        access_token = ret["ret"]

        sub_profiles[profile] = dict(
            endpoint_url=endpoint_url,
            headers={"vmware-api-session-id": f"{access_token}"},
        )
    return sub_profiles
